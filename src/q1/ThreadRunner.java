package q1;

import java.util.LinkedList;

public class ThreadRunner {
	public static void main(String[] args){
		LinkedList<Integer> link = new LinkedList<Integer>();
		AddList addlist = new AddList(link,5);
		RemoveList removelist = new RemoveList(link,5);
		Thread t1 = new Thread(addlist);
		Thread t2 = new Thread(removelist);
		
		t1.run();
		t2.run();
		
	}
}
