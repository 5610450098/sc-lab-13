package q2;

public class Main {
	public static void main(String[] args){
		Queue_List q = new Queue_List();
		Consumer consumer = new Consumer(q);
		Producer producer = new Producer(q);
		
		Thread con = new Thread(consumer);
		Thread pro = new Thread(producer);
		
		con.start();
		pro.start();
	}

}
