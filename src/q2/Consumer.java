package q2;

public class Consumer implements Runnable {
	Queue_List q;
	
	public Consumer(Queue_List q){
		this.q = q;
	}
	public void run(){
		for(int i = 0;i<100;i++){
			try {
				dequeue();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void dequeue(){
		String word = q.dequeue();
		System.out.println(word);
	}


}
