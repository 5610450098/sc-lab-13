package q2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue_List {
	private Queue<String> q = new LinkedList<String>();
	private Lock EmptyQueue;
	private Condition condition;
	private int count;
	public String date;
	public Queue_List(){
		EmptyQueue = new ReentrantLock();
		condition = EmptyQueue.newCondition();
		count = 0;
	}
	
	public String dequeue(){
		EmptyQueue.lock();
		try{
			while(q.peek()==null){
				condition.await();	
			}
			date = q.poll();
			condition.signalAll();
			return date;
		}catch(InterruptedException e){
			System.err.println("Interrupted Error");
		}finally{
			EmptyQueue.unlock();
			return date;
		}
	}
	
	public void enqueue(String word){
		EmptyQueue.lock();
		try{
			while(q.size()>=10){
				condition.await();
			}
			q.add(word);
			condition.signalAll(); }
		catch(InterruptedException e){
			System.err.println("Interrupted Error");}
		finally{
			EmptyQueue.unlock();}
	}
	
	public Queue<String> getQueue(){
		return q;
	}
}
